Test tool: Postman

Task 1

Create a test to validate your initial setup via Trello REST API and Postman

Use the Trello API key and token
Follow the best testing practices
Validate authentication
Task 2

Create a board in Trello via Trello REST API and Postman

Use the Trello API key and token
Follow the best practices and naming conventions
Validate board creation
Task 3

Create a list in Trello via Trello REST API and Postman

Use the Trello API key and token
Follow the best practices and naming conventions
Validate list creation
Task 4

Create a card in Trello via Trello REST API and Postman

Use the Trello API key and token
Reuse the newly created list in Task 3
Follow the best practices and naming conventions
Validate card creation
Task 5

Update the card in Trello via Trello REST API and Postman

Use the Trello API key and token
Set/update card name, description
Follow the best practices and naming conventions
Validate card name and description
Advanced tasks

Task 1

Create a card in Trello via Trello REST API and Postman

Use the Trello API key and token
Use any of the default board lists
Follow the best practices and naming conventions
Validate card creation
Task 2

Update the card in Trello via Trello REST API and Postman

Use the Trello API key and token
Set cover color
Follow the best practices and naming conventions
Validate cover color
Task 3

Update the card in Trello via Trello REST API and Postman

Use the Trello API key and token
Move a card to another list
Follow the best practices and naming conventions
Validate new card list
